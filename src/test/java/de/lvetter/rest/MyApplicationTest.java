package de.lvetter.rest;

import de.lvetter.Main;
import io.undertow.Undertow;
import io.undertow.server.handlers.resource.ClassPathResourceManager;
import io.undertow.servlet.Servlets;
import io.undertow.servlet.api.DeploymentInfo;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.jboss.resteasy.plugins.server.undertow.UndertowJaxrsServer;
import org.jboss.resteasy.spi.ResteasyDeployment;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;


class MyApplicationTest {

    private static UndertowJaxrsServer server;

    @BeforeAll
    static void setup() {
        server = new UndertowJaxrsServer();

        ResteasyDeployment deployment = new ResteasyDeployment();
        deployment.setApplicationClass(MyApplication.class.getName());
        deployment.setInjectorFactoryClass("org.jboss.resteasy.cdi.CdiInjectorFactory");

        DeploymentInfo deploymentInfo = server.undertowDeployment(deployment)
                .setClassLoader(getMainClassLoader())
                .setResourceManager(new ClassPathResourceManager(getMainClassLoader()))
                .setDeploymentName("demo")
                .setContextPath("")
                .addListeners(Servlets.listener(org.jboss.weld.environment.servlet.Listener.class));

        server.deploy(deploymentInfo);

        server.start(Undertow.builder().addHttpListener(8085, "localhost"));
    }

    @AfterAll
    static void teardown() {
        server.stop();
    }

    @Test
    void test() {
        String s = new ResteasyClientBuilder()
                .build()
                .target("http://localhost:8085")
                .path("demo/print")
                .request()
                .get(String.class);

        assertThat(s).isEqualTo("Hello World");
    }

    private static ClassLoader getMainClassLoader() {
        return Main.class.getClassLoader();
    }
}
