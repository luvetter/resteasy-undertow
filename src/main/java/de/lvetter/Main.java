package de.lvetter;

import de.lvetter.rest.MyApplication;
import io.undertow.Undertow;
import io.undertow.server.handlers.resource.ClassPathResourceManager;
import io.undertow.servlet.Servlets;
import io.undertow.servlet.api.DeploymentInfo;
import org.jboss.resteasy.plugins.server.undertow.UndertowJaxrsServer;
import org.jboss.resteasy.spi.ResteasyDeployment;

public class Main {
    private static final UndertowJaxrsServer server = new UndertowJaxrsServer();

    public static void main(String[] args) {

        Undertow.Builder serverBuilder = Undertow.builder() //
                .addHttpListener(8085, "0.0.0.0");
        ResteasyDeployment deployment = new ResteasyDeployment();
        deployment.setApplicationClass(MyApplication.class.getName());
        deployment.setInjectorFactoryClass("org.jboss.resteasy.cdi.CdiInjectorFactory");

        DeploymentInfo deploymentInfo = server.undertowDeployment(deployment) //
                .setClassLoader(getMainClassLoader()) //
                .setResourceManager(new ClassPathResourceManager(getMainClassLoader())) //
                .setContextPath("") //
                .setDeploymentName("My Application") //
                .addListeners(Servlets.listener(org.jboss.weld.environment.servlet.Listener.class));

        server.deploy(deploymentInfo);
        server.start(serverBuilder);
    }

    private static ClassLoader getMainClassLoader() {
        return Main.class.getClassLoader();
    }
}
