package de.lvetter.service;

import javax.enterprise.context.Dependent;
import javax.enterprise.inject.Alternative;
import javax.enterprise.inject.Specializes;

@Specializes
@Dependent
@Alternative
public class TestPrinter extends Printer {

    @Override
    public String helloWorld() {
        return "Hello Test";
    }
}
