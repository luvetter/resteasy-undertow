package de.lvetter.service;

import javax.enterprise.context.Dependent;

@Dependent
public class Printer {

    public String helloWorld() {
        return "Hello World";
    }
}
